package com.thecrackertechnology.andrax;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;

import com.thecrackertechnology.dragonterminal.bridge.Bridge;

public class Dco_Wireless_Hacking extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);


        setContentView(R.layout.dco_wireless_hacking);


        CardView cardviewaircrack = findViewById(R.id.card_view_aircrack);
        CardView cardviewcowpatty = findViewById(R.id.card_view_cowpatty);
        CardView cardviewmdk3 = findViewById(R.id.card_view_mdk3);
        CardView cardviewmdk4 = findViewById(R.id.card_view_mdk4);
        CardView cardviewbully = findViewById(R.id.card_view_bully);
        CardView cardviewreaver = findViewById(R.id.card_view_reaver);
        CardView cardviewwash = findViewById(R.id.card_view_wash);
        CardView cardviewhostapd = findViewById(R.id.card_view_hostapd);
        CardView cardvieweaphammer = findViewById(R.id.card_view_eaphammer);
        CardView cardviewbluesnarfer = findViewById(R.id.card_view_bluesnarfer);
        CardView cardviewcrackle = findViewById(R.id.card_view_crackle);
        CardView cardviewblescan = findViewById(R.id.card_view_blescan);
        CardView cardviewbtlejack = findViewById(R.id.card_view_btlejack);
        CardView cardviewbtscanner = findViewById(R.id.card_view_btscanner);
        CardView cardviewhcxdumptool = findViewById(R.id.card_view_hcxdumptool);
        CardView cardviewwifiarp = findViewById(R.id.card_view_wifiarp);
        CardView cardviewwifidns = findViewById(R.id.card_view_wifidns);
        CardView cardviewwifiping = findViewById(R.id.card_view_wifiping);
        CardView cardviewwifitap = findViewById(R.id.card_view_wifitap);
        CardView cardvieweapmd5pass = findViewById(R.id.card_view_eapmd5pass);
        CardView cardviewhciconfig = findViewById(R.id.card_view_hciconfig);
        CardView cardviewbluetoothctl = findViewById(R.id.card_view_bluetoothctl);
        CardView cardviewgatttool = findViewById(R.id.card_view_gatttool);
        CardView cardviewpixiewps = findViewById(R.id.card_view_pixiewps);
        CardView cardviewwifite2 = findViewById(R.id.card_view_wifite2);

        cardviewaircrack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("aircrack-ng");

            }
        });

        /**
         *
         * Help me, i'm dying...
         *
         **/

        cardviewcowpatty.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("cowpatty");

            }
        });

        cardviewmdk3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("mdk3");

            }
        });

        cardviewmdk4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("mdk4");

            }
        });

        cardviewbully.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("bully");

            }
        });

        cardviewreaver.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("reaver");

            }
        });

        cardviewwash.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("wash");

            }
        });

        cardvieweaphammer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("eaphammer -h");

            }
        });

        cardviewhostapd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("hostapd -h");

            }
        });

        cardviewbluesnarfer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("bluesnarfer");

            }
        });

        cardviewcrackle.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("crackle");

            }
        });

        cardviewblescan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("blescan -h");

            }
        });

        cardviewbtlejack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("btlejack");

            }
        });

        cardviewbtscanner.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("btscanner -h");

            }
        });

        cardviewhcxdumptool.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("hcxdumptool");

            }
        });

        cardviewwifiarp.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("wifiarp");

            }
        });

        cardviewwifidns.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("wifidns");

            }
        });

        cardviewwifiping.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("wifiping");

            }
        });

        cardviewwifitap.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("wifitap");

            }
        });

        cardvieweapmd5pass.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("eapmd5pass");

            }
        });

        cardviewhciconfig.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("hciconfig");

            }
        });

        cardviewbluetoothctl.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("bluetoothctl");

            }
        });

        cardviewgatttool.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("gatttool");

            }
        });

        cardviewpixiewps.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("pixiewps");

            }
        });

        cardviewwifite2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("wifite");

            }
        });

    }

    public void run_hack_cmd(String cmd) {

        Intent intent = Bridge.createExecuteIntent(cmd);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);

    }

    @Override
    public void onPause() {

        super.onPause();
        finish();
    }
}
